# README #

### Massive interactive test case ###

This repository contains solution of my test case.

Solution consist of three components. 
* Console application for uploading graph loaded from source xml files.
* Web service for storing, retrieving a calculating shortest path.
* Web application for presenting graph to user

### Deployment ###

* Upload web application and web services to server
* Configure database connection for services
* Configure correct addresses of web services in configs for web application and console application
* Run console application with correct source folder specified to load up graph
