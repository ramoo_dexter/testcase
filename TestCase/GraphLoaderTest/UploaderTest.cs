﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphLoader.Store;
using GraphLoaderTest.Mock;

namespace GraphLoaderTest
{
	/// <summary>
	/// Test class for testing uploader.
	/// </summary>
	[TestClass]
	public class UploaderTest
	{
		/// <summary>
		/// Test for testing null store providen.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void NullStoreProviden()
		{
			var uploader = new Uploader();
			uploader.UploadGraph(null, new DummyGraphLoader());
		}

		/// <summary>
		/// Test for testing null loader providen.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void NullLoaderProviden()
		{
			var uploader = new Uploader();
			uploader.UploadGraph(new DummyGraphStore(), null);
		}
	}
}
