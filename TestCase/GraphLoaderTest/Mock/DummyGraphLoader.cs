﻿using GraphLoader.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphLoader.Model;

namespace GraphLoaderTest.Mock
{
	/// <summary>
	/// Dummy loader, used to mock.
	/// </summary>
	public class DummyGraphLoader : IGraphLoader
	{
		/// <summary>
		/// Loads edges. Method does not contain any loading logic.
		/// </summary>
		/// <returns>Empty list of edges</returns>
		public IEnumerable<Edge> LoadEdges()
		{
			return new List<Edge>();
		}

		/// <summary>
		/// Loads nodes. Method does not contain any loading logic.
		/// </summary>
		/// <returns>Empty list of nodes</returns>
		public IEnumerable<Node> LoadNodes()
		{
			return new List<Node>();
		}
	}
}
