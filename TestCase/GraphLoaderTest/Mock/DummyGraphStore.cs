﻿using GraphLoader.Model;
using GraphLoader.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphLoaderTest.Mock
{
	/// <summary>
	/// Class for mocking graph store. Counts number of items uploaded.
	/// </summary>
	public class DummyGraphStore : IGraphStore
	{
		/// <summary>
		/// Number of nodes added
		/// </summary>
		public Int32 NodesAdded { get; private set; }

		/// <summary>
		/// Number of edges added
		/// </summary>
		public Int32 EdgesAdded { get; private set; }

		/// <summary>
		/// Constructor
		/// </summary>
		public DummyGraphStore()
		{
			NodesAdded = 0;
			EdgesAdded = 0;
		}

		/// <summary>
		/// Adds edge. Increases mnumber of edges by one.
		/// </summary>
		/// <param name="idFirst">First node id</param>
		/// <param name="idSecond">Second node id</param>
		public void AddEdge(int idFirst, int idSecond)
		{
			NodesAdded++;
		}

		/// <summary>
		/// Adds node. Increases mnumber of nodes by one.
		/// </summary>
		/// <param name="id">Id of node</param>
		/// <param name="label">Label of node</param>
		public void AddNode(int id, string label)
		{
			EdgesAdded++;
		}

		/// <summary>
		/// Clears store. Resets numbers of nodes and edges to zero.
		/// </summary>
		public void Clear()
		{
			NodesAdded = 0;
			EdgesAdded = 0;
		}
	}
}
