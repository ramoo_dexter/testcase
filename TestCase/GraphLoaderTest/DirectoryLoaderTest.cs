﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphLoader.Store;
using GraphLoaderTest.Mock;
using System.IO;
using System.Linq;

namespace GraphLoaderTest
{
	/// <summary>
	/// Test class for testing directory loading.
	/// </summary>
	[TestClass]
	public class LoaderTest
	{
		/// <summary>
		/// Test for testing invalid path providen.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void InvalidPathProviden()
		{
			var loader = new DirectoryLoader("notexisting");
		}

		/// <summary>
		/// Test for testing valid path providen.
		/// </summary>
		[TestMethod]
		public void ValidPathProviden()
		{
			var path = Path.Combine(Environment.CurrentDirectory, "Data");
			var loader = new DirectoryLoader(path);

			Assert.AreEqual(10, loader.LoadNodes().Count());
			Assert.AreEqual(18, loader.LoadEdges().Count());
		}
	}
}
