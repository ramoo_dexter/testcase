﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphServiceTest.Mock;
using GraphService.Contracts;
using System.Linq;
using GraphService.Service;

namespace GraphServiceTest
{
	/// <summary>
	/// Test class for testing graph service facade.
	/// </summary>
	[TestClass]
	public class GraphServiceFacadeTest
	{
		/// <summary>
		/// Test add node with null argument.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void AddNodeNullArgument()
		{
			var storage = new DummyGraphStoringService();
			var facade = new GraphServiceFacade(storage);

			facade.AddNode(null, true);
		}

		/// <summary>
		/// Tests add node with valid argument.
		/// </summary>
		[TestMethod]
		public void AddNode()
		{
			var storage = new DummyGraphStoringService();
			var facade = new GraphServiceFacade(storage);
			Assert.AreEqual(0, storage.NodesInserted.Count);

			facade.AddNode(new NodeContract { Id = 1, Label = "test" }, true);
			Assert.AreEqual(1, storage.NodesInserted.Count);
			Assert.AreEqual(1, storage.NodesInserted[0].ID);
		}

		/// <summary>
		/// Tests adding existing node without forced skip existing nodes.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void AddExistingNodeWithoutSkipExistingNode()
		{
			var storage = new DummyGraphStoringService();
			var facade = new GraphServiceFacade(storage);
			Assert.AreEqual(0, storage.NodesInserted.Count);

			facade.AddNode(new NodeContract { Id = 1, Label = "test" }, true);
			Assert.AreEqual(1, storage.NodesInserted.Count);

			facade.AddNode(new NodeContract { Id = 1, Label = "test" }, false);
		}

		/// <summary>
		/// Tests adding existing node with forced skip existing nodes.
		/// </summary>
		[TestMethod]
		public void AddExistingNodeWithSkipExistingNode()
		{
			var storage = new DummyGraphStoringService();
			var facade = new GraphServiceFacade(storage);
			Assert.AreEqual(0, storage.NodesInserted.Count);

			facade.AddNode(new NodeContract { Id = 1, Label = "test" }, true);
			Assert.AreEqual(1, storage.NodesInserted.Count);

			facade.AddNode(new NodeContract { Id = 1, Label = "test" }, true);
			Assert.AreEqual(1, storage.NodesInserted.Count);
		}

		/// <summary>
		/// Test add edge with null argument
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void AddEdgeNullArgument()
		{
			var storage = new DummyGraphStoringService();
			var facade = new GraphServiceFacade(storage);
			facade.AddEdge(null, true);
		}

		/// <summary>
		/// Tests adding edge with nonexisting first node.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void AddEdgeNonExistingFirstNode()
		{
			var storage = new DummyGraphStoringService();
			var facade = new GraphServiceFacade(storage);

			facade.AddNode(new NodeContract { Id = 1, Label = "test" }, true);
			Assert.AreEqual(1, storage.NodesInserted.Count);
			Assert.AreEqual(1, storage.NodesInserted[0].ID);

			facade.AddEdge(new EdgeContract { IdFirst = 1, IdSecond = 2 }, true);
		}

		/// <summary>
		///  Tests adding edge with nonexisting second node.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void AddEdgeNonExistingSecondNode()
		{
			var storage = new DummyGraphStoringService();
			var facade = new GraphServiceFacade(storage);

			facade.AddNode(new NodeContract { Id = 1, Label = "test" }, true);
			Assert.AreEqual(1, storage.NodesInserted.Count);
			Assert.AreEqual(1, storage.NodesInserted[0].ID);

			facade.AddEdge(new EdgeContract { IdFirst = 2, IdSecond = 1 }, true);
		}

		/// <summary>
		/// Tests add edge with valid arguments.
		/// </summary>
		[TestMethod]
		public void AddEdge()
		{
			var storage = new DummyGraphStoringService();
			var facade = new GraphServiceFacade(storage);

			facade.AddNode(new NodeContract { Id = 1, Label = "test1" }, true);
			facade.AddNode(new NodeContract { Id = 2, Label = "test2" }, true);
			Assert.AreEqual(0, storage.EdgesInserted.Count);

			facade.AddEdge(new EdgeContract { IdFirst = 1, IdSecond = 2 }, true);
			Assert.AreEqual(1, storage.EdgesInserted.Count);
			Assert.AreEqual(1, storage.EdgesInserted[0].FirstNode.ID);
			Assert.AreEqual(2, storage.EdgesInserted[0].SecondNode.ID);
		}

		/// <summary>
		/// Tests adding existing edge without forced skip existing nodes.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void AddExistingEdgeWithoutSkipExistingEdge()
		{
			var storage = new DummyGraphStoringService();
			var facade = new GraphServiceFacade(storage);

			facade.AddNode(new NodeContract { Id = 1, Label = "test1" }, true);
			facade.AddNode(new NodeContract { Id = 2, Label = "test2" }, true);
			Assert.AreEqual(0, storage.EdgesInserted.Count);

			facade.AddEdge(new EdgeContract { IdFirst = 1, IdSecond = 2 }, true);
			Assert.AreEqual(1, storage.EdgesInserted.Count);
			Assert.AreEqual(1, storage.EdgesInserted[0].FirstNode.ID);
			Assert.AreEqual(2, storage.EdgesInserted[0].SecondNode.ID);

			facade.AddEdge(new EdgeContract { IdFirst = 1, IdSecond = 2 }, false);
		}

		/// <summary>
		/// Tests adding existing edge with forced skip existing nodes.
		/// </summary>
		[TestMethod]
		public void AddExistingEdgeWithSkipExistingEdge()
		{
			var storage = new DummyGraphStoringService();
			var facade = new GraphServiceFacade(storage);

			facade.AddNode(new NodeContract { Id = 1, Label = "test1" }, true);
			facade.AddNode(new NodeContract { Id = 2, Label = "test2" }, true);
			Assert.AreEqual(0, storage.EdgesInserted.Count);

			facade.AddEdge(new EdgeContract { IdFirst = 1, IdSecond = 2 }, true);
			Assert.AreEqual(1, storage.EdgesInserted.Count);
			Assert.AreEqual(1, storage.EdgesInserted[0].FirstNode.ID);
			Assert.AreEqual(2, storage.EdgesInserted[0].SecondNode.ID);

			facade.AddEdge(new EdgeContract { IdFirst = 1, IdSecond = 2 }, true);
			Assert.AreEqual(1, storage.EdgesInserted.Count);
			Assert.AreEqual(1, storage.EdgesInserted[0].FirstNode.ID);
			Assert.AreEqual(2, storage.EdgesInserted[0].SecondNode.ID);
		}

		/// <summary>
		/// Test clearing of graph service facade.
		/// </summary>
		[TestMethod]
		public void Clear()
		{
			var storage = new DummyGraphStoringService();
			var facade = new GraphServiceFacade(storage);

			facade.AddNode(new NodeContract { Id = 1, Label = "test1" }, true);
			facade.AddNode(new NodeContract { Id = 2, Label = "test2" }, true);
			facade.AddEdge(new EdgeContract { IdFirst = 1, IdSecond = 2 }, true);
			Assert.AreEqual(2, storage.NodesInserted.Count);
			Assert.AreEqual(1, storage.EdgesInserted.Count);

			facade.Clear();
			Assert.AreEqual(0, storage.NodesInserted.Count);
			Assert.AreEqual(0, storage.EdgesInserted.Count);
		}

		/// <summary>
		/// Tests getting node with valid id.
		/// </summary>
		[TestMethod]
		public void GetNodeExistingId()
		{
			var storage = new DummyGraphStoringService();
			var facade = new GraphServiceFacade(storage);

			facade.AddNode(new NodeContract { Id = 1, Label = "test1" }, true);
			facade.AddNode(new NodeContract { Id = 2, Label = "test2" }, true);

			var node = facade.GetNode(1);
			Assert.IsNotNull(node);
			Assert.AreEqual(1, node.Id);

			node = facade.GetNode(2);
			Assert.IsNotNull(node);
			Assert.AreEqual(2, node.Id);
		}

		/// <summary>
		/// Tests getting node with invalid id.
		/// </summary>
		[TestMethod]
		public void GetNodeNonExistingId()
		{
			var storage = new DummyGraphStoringService();
			var facade = new GraphServiceFacade(storage);

			facade.AddNode(new NodeContract { Id = 1, Label = "test1" }, true);
			facade.AddNode(new NodeContract { Id = 2, Label = "test2" }, true);

			var node = facade.GetNode(3);
			Assert.IsNull(node);
		}

		/// <summary>
		/// Tests get graph method using mock graph storing service.
		/// </summary>
		[TestMethod]
		public void GetGraph()
		{
			var storage = new DummyGraphStoringService();
			var facade = new GraphServiceFacade(storage);

			// 1 - 2 - 3 4
			facade.AddNode(new NodeContract { Id = 1, Label = "test1" }, true);
			facade.AddNode(new NodeContract { Id = 2, Label = "test2" }, true);
			facade.AddNode(new NodeContract { Id = 3, Label = "test3" }, true);
			facade.AddNode(new NodeContract { Id = 4, Label = "test4" }, true);
			facade.AddEdge(new EdgeContract { IdFirst = 1, IdSecond = 2 }, true);
			facade.AddEdge(new EdgeContract { IdFirst = 3, IdSecond = 2 }, true);

			var graph = facade.GetGraph();

			Assert.IsNotNull(graph);
			Assert.AreEqual(4, graph.Nodes.Count);
			Assert.IsTrue(graph.Nodes.Any(n => n.Id == 1));
			Assert.IsTrue(graph.Nodes.Any(n => n.Id == 2));
			Assert.IsTrue(graph.Nodes.Any(n => n.Id == 3));
			Assert.IsTrue(graph.Nodes.Any(n => n.Id == 4));
			Assert.AreEqual(2, graph.Edges.Count);
			Assert.IsTrue(graph.Edges.Any(e => (e.IdFirst == 1 && e.IdSecond == 2) || (e.IdFirst == 2 && e.IdSecond == 1)));
			Assert.IsTrue(graph.Edges.Any(e => (e.IdFirst == 2 && e.IdSecond == 3) || (e.IdFirst == 3 && e.IdSecond == 2)));
		}
	}
}
