﻿using GraphService.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphService.Model;

namespace GraphServiceTest.Mock
{
	/// <summary>
	/// Class representing mock node and edge storage. Nodes and edges are stored in memory.
	/// </summary>
	public class DummyGraphStoringService : IGraphStoringService
	{
		/// <summary>
		/// Nodes stored
		/// </summary>
		public List<Node> NodesInserted { get; private set; }

		/// <summary>
		/// Edges stored
		/// </summary>
		public List<Edge> EdgesInserted { get; private set; }

		/// <summary>
		/// Constructor
		/// </summary>
		public DummyGraphStoringService()
		{
			NodesInserted = new List<Node>();
			EdgesInserted = new List<Edge>();
		}

		/// <summary>
		/// Adds edge to store
		/// </summary>
		/// <param name="edge">Edge to add</param>
		public void AddEdge(Edge edge)
		{
			EdgesInserted.Add(edge);
		}

		/// <summary>
		/// Adds node to store
		/// </summary>
		/// <param name="node">Node to add</param>
		public void AddNode(Node node)
		{
			NodesInserted.Add(node);
		}

		/// <summary>
		/// Clears storage
		/// </summary>
		public void Clear()
		{
			NodesInserted.Clear();
			EdgesInserted.Clear();
		}

		/// <summary>
		/// Returns stored edges
		/// </summary>
		/// <returns>Returns stored edges</returns>
		public IEnumerable<Edge> GetEdges()
		{
			return EdgesInserted;
		}

		/// <summary>
		/// Returns node with specified id
		/// </summary>
		/// <param name="id">Id of stored node</param>
		/// <returns>Node with specified id or null if not found</returns>
		public Node GetNode(int id)
		{
			return NodesInserted.Where(n => n.ID == id).FirstOrDefault();
		}

		/// <summary>
		/// Returns stored nodes
		/// </summary>
		/// <returns>Returns stored nodes</returns>
		public IEnumerable<Node> GetNodes()
		{
			return NodesInserted;
		}
	}
}
