﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace GraphService.Contracts
{
	/// <summary>
	/// Class representing edge data
	/// </summary>
	[DataContract]
	public class EdgeContract
	{
		/// <summary>
		/// Id of first node
		/// </summary>
		[DataMember]
		public Int32 IdFirst { get; set; }

		/// <summary>
		/// Id of second node
		/// </summary>
		[DataMember]
		public Int32 IdSecond { get; set; }
	}
}