﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace GraphService.Contracts
{
	/// <summary>
	/// Class representing result data of calculation of shortest path.
	/// </summary>
	[DataContract]
	public class CalculateContract
	{
		/// <summary>
		/// Source node
		/// </summary>
		[DataMember]
		public NodeContract From { get; set; }

		/// <summary>
		/// Target node
		/// </summary>
		[DataMember]
		public NodeContract To { get; set; }

		/// <summary>
		/// Array of nodes sorted from source to target node
		/// </summary>
		[DataMember]
		public NodeContract[] Path { get; set; }
	}
}