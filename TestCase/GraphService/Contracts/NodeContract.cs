﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace GraphService.Contracts
{
	/// <summary>
	/// Class representing node data
	/// </summary>
	[DataContract]
	public class NodeContract
	{
		/// <summary>
		/// Id of node
		/// </summary>
		[DataMember]
		public Int32 Id { get; set; }

		/// <summary>
		/// Label of node
		/// </summary>
		[DataMember]
		public String Label { get; set; }
	}
}