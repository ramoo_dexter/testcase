﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace GraphService.Contracts
{
	/// <summary>
	/// Class representing graph data
	/// </summary>
	[DataContract]
	public class GraphContract
	{
		/// <summary>
		/// List of all nodes in graph
		/// </summary>
		[DataMember]
		public List<NodeContract> Nodes { get; set; }

		/// <summary>
		/// List of all edges between nodes in graph
		/// </summary>
		[DataMember]
		public List<EdgeContract> Edges { get; set; }
	}
}