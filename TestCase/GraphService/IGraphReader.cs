﻿using GraphService.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace GraphService
{
	/// <summary>
	/// Interface of graph retrieving service
	/// </summary>
	[ServiceContract]
	public interface IGraphReader
	{
		/// <summary>
		/// Method for getting complete graph from storage
		/// </summary>
		/// <returns>Graph data with nodes and edges in graph</returns>
		[OperationContract]
		GraphContract GetGraph();

		/// <summary>
		/// Method for returning node by id.
		/// </summary>
		/// <param name="id">Id of node to return</param>
		/// <returns>Node if found, otherwise null</returns>
		[OperationContract]
		NodeContract GetNode(int id);
	}
}
