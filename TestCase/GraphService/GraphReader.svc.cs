﻿using GraphService.Contracts;
using GraphService.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace GraphService
{
	/// <summary>
	/// Class representing graph retieving service 
	/// </summary>
	public class GraphReader : IGraphReader
	{
		/// <summary>
		/// Graph service facade to use
		/// </summary>
		protected IGraphServiceFacade _graphServiceFacade;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="graphServiceFacade">Graph service facade to use</param>
		public GraphReader(IGraphServiceFacade graphServiceFacade)
		{
			if (graphServiceFacade == null)
				throw new ArgumentNullException("graphServiceFacade");

			_graphServiceFacade = graphServiceFacade;
		}

		/// <summary>
		/// Method for getting graph
		/// </summary>
		/// <returns>Graph</returns>
		public GraphContract GetGraph()
		{
			return _graphServiceFacade.GetGraph();
		}

		/// <summary>
		/// Method for getting node by id
		/// </summary>
		/// <param name="id"></param>
		/// <returns>Node by id, null if not found</returns>
		public NodeContract GetNode(int id)
		{
			return _graphServiceFacade.GetNode(id);
		}
	}
}
