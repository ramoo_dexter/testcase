﻿using GraphService.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GraphService
{
	/// <summary>
	/// Interface of graph calculation service
	/// </summary>
	[ServiceContract]
	public interface IGraphService
	{
		/// <summary>
		/// Method for calculating shortest path between two nodes in graph.
		/// </summary>
		/// <param name="idStart">Id of start node</param>
		/// <param name="idEnd">Id of end node</param>
		/// <returns></returns>
		[OperationContract]
		CalculateContract CalculatePath(int idStart, int idEnd);
	}
}
