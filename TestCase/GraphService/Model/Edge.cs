﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GraphService.Model
{
	/// <summary>
	/// Class representing edge
	/// </summary>
	public class Edge
	{
		/// <summary>
		/// Id of edge
		/// </summary>
		[Key]
		public Guid ID { get; private set; }

		/// <summary>
		/// First node of edge
		/// </summary>
		[Required]
		public virtual Node FirstNode { get; private set; }

		/// <summary>
		/// Second node of edge
		/// </summary>
		[Required]
		public virtual Node SecondNode { get; private set; }

		/// <summary>
		/// Constructor
		/// </summary>
		private Edge() { }

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="firstNode">First node of edge</param>
		/// <param name="secondNode">Second node of edge</param>
		public Edge(Node firstNode, Node secondNode)
		{
			if (firstNode == null)
				throw new ArgumentNullException("firstNode");

			if (secondNode == null)
				throw new ArgumentNullException("secondNode");

			ID = Guid.NewGuid();
			FirstNode = firstNode;
			SecondNode = secondNode;
		}
	}
}