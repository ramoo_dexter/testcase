﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GraphService.Model
{
	/// <summary>
	/// Class representing node
	/// </summary>
	public class Node
	{
		/// <summary>
		/// Node id
		/// </summary>
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public Int32 ID { get; set; }

		/// <summary>
		/// Node label
		/// </summary>
		[Required]
		public String Label { get; set; }
	}
}