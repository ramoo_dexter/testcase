﻿using GraphService.DAL;
using GraphService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using GraphService.Contracts;
using GraphService.Service;

namespace GraphService
{
	/// <summary>
	/// Class representing graph calculation shortest path service 
	/// </summary>
	public class GraphService : IGraphService
	{
		/// <summary>
		/// Graph service facade to use
		/// </summary>
		protected IGraphServiceFacade _graphServiceFacade;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="graphServiceFacade">Graph service facade to use</param>
		public GraphService(IGraphServiceFacade graphServiceFacade)
		{
			if (graphServiceFacade == null)
				throw new ArgumentNullException("graphServiceFacade");

			_graphServiceFacade = graphServiceFacade;
		}

		/// <summary>
		/// Calculates shortest path between two nodes in graph
		/// </summary>
		/// <param name="idStart">Id of start node</param>
		/// <param name="idEnd">Id of end node</param>
		/// <returns>Calculated path result or null if no path is found</returns>
		public CalculateContract CalculatePath(int idStart, int idEnd)
		{
			return _graphServiceFacade.CalculatePath(idStart, idEnd);
		}
	}
}
