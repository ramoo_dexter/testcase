﻿using GraphService.DAL;
using GraphService.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;

namespace GraphService.DependencyInjection
{
	/// <summary>
	/// Service host factory used register di container services.
	/// </summary>
	public class UnityServiceHostFactory : ServiceHostFactory
	{
		/// <summary>
		/// DI container
		/// </summary>
		private readonly IUnityContainer container;

		/// <summary>
		/// Constructor
		/// </summary>
		public UnityServiceHostFactory()
		{
			container = new UnityContainer();
			RegisterTypes(container);
		}

		/// <summary>
		/// Creates unity service host
		/// </summary>
		/// <param name="serviceType">Service type</param>
		/// <param name="baseAddresses">Base addresses</param>
		/// <returns>Unity service host</returns>
		protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
		{
			return new UnityServiceHost(this.container, serviceType, baseAddresses);
		}

		/// <summary>
		/// Registers DI container service types
		/// </summary>
		/// <param name="container">Container to register services</param>
		private void RegisterTypes(IUnityContainer container)
		{
			if (container == null)
				throw new ArgumentNullException("container");

			container.RegisterInstance<IGraphStoringService>(new ContextStoringService(new GraphStoreContext()));
			container.RegisterInstance<IGraphServiceFacade>(new GraphServiceFacade(container.Resolve<IGraphStoringService>()));
		}
	}
}