﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Web;

namespace GraphService.DependencyInjection
{
	/// <summary>
	/// Class representing unity instance provider. Provides instances of specific services.
	/// </summary>
	public class UnityInstanceProvider : IInstanceProvider, IContractBehavior
	{
		/// <summary>
		/// Unity container
		/// </summary>
		private readonly IUnityContainer container;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="container">Unity container to use</param>
		public UnityInstanceProvider(IUnityContainer container)
		{
			if (container == null)
				throw new ArgumentNullException("container");

			this.container = container;
		}

		/// <summary>
		/// Returns service instance
		/// </summary>
		/// <param name="instanceContext"></param>
		/// <param name="message"></param>
		/// <returns></returns>
		public object GetInstance(InstanceContext instanceContext, Message message)
		{
			return this.GetInstance(instanceContext);
		}

		/// <summary>
		/// Returns service instance
		/// </summary>
		/// <param name="instanceContext"></param>
		/// <returns></returns>
		public object GetInstance(InstanceContext instanceContext)
		{
			return this.container.Resolve(instanceContext.Host.Description.ServiceType);
		}

		/// <summary>
		/// Releases instance
		/// </summary>
		/// <param name="instanceContext"></param>
		/// <param name="instance"></param>
		public void ReleaseInstance(InstanceContext instanceContext, object instance) { }

		/// <summary>
		/// Adds binding parameters
		/// </summary>
		/// <param name="contractDescription"></param>
		/// <param name="endpoint"></param>
		/// <param name="bindingParameters"></param>
		public void AddBindingParameters(ContractDescription contractDescription, ServiceEndpoint endpoint, BindingParameterCollection bindingParameters) { }

		/// <summary>
		/// Applies client behavior
 		/// </summary>
		/// <param name="contractDescription"></param>
		/// <param name="endpoint"></param>
		/// <param name="clientRuntime"></param>
		public void ApplyClientBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, ClientRuntime clientRuntime) { }

		/// <summary>
		/// Applies dispatch behavior
		/// </summary>
		/// <param name="contractDescription"></param>
		/// <param name="endpoint"></param>
		/// <param name="dispatchRuntime"></param>
		public void ApplyDispatchBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, DispatchRuntime dispatchRuntime)
		{
			dispatchRuntime.InstanceProvider = this;
		}

		/// <summary>
		/// Validates
		/// </summary>
		/// <param name="contractDescription"></param>
		/// <param name="endpoint"></param>
		public void Validate(ContractDescription contractDescription, ServiceEndpoint endpoint) { }
	}
}