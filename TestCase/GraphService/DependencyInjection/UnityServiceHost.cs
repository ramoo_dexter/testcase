﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;

namespace GraphService.DependencyInjection
{
	/// <summary>
	/// Class representing unity service host
	/// </summary>
	public class UnityServiceHost : ServiceHost
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="container">Unity container</param>
		/// <param name="serviceType">Service type</param>
		/// <param name="baseAddresses">Base addresses</param>
		public UnityServiceHost(IUnityContainer container, Type serviceType, params Uri[] baseAddresses) : base(serviceType, baseAddresses)
		{
			if (container == null)
				throw new ArgumentNullException("container");

			foreach (var cd in this.ImplementedContracts.Values)
			{
				cd.Behaviors.Add(new UnityInstanceProvider(container));
			}
		}
	}
}