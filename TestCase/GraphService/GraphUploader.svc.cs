﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using GraphService.Contracts;
using GraphService.Service;

namespace GraphService
{
	/// <summary>
	/// Class representing graph uploading service 
	/// </summary>
	public class GraphUploader : IGraphUploader
	{
		/// <summary>
		/// Graph service facade to use
		/// </summary>
		protected IGraphServiceFacade _graphServiceFacade;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="graphServiceFacade">Graph service facade to use</param>
		public GraphUploader(IGraphServiceFacade graphServiceFacade)
		{
			if (graphServiceFacade == null)
				throw new ArgumentNullException("graphServiceFacade");

			_graphServiceFacade = graphServiceFacade;
		}

		/// <summary>
		///  Adds edge to graph
		/// </summary>
		/// <param name="edge">Edge to add</param>
		/// <param name="skipExisting">Skip if edge is already present, exception is otherwise thrown</param>
		public void AddEdge(EdgeContract edge, bool skipExisting)
		{
			_graphServiceFacade.AddEdge(edge, skipExisting);
		}

		/// <summary>
		/// Adds node to graph
		/// </summary>
		/// <param name="node">Node to add</param>
		/// <param name="skipExisting">Skip if node is already present, exception is otherwise thrown</param>
		public void AddNode(NodeContract node, bool skipExisting)
		{
			_graphServiceFacade.AddNode(node, skipExisting);
		}

		/// <summary>
		/// Clears stored graph
		/// </summary>
		public void Clear()
		{
			_graphServiceFacade.Clear();
		}
	}
}
