﻿using GraphService.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace GraphService
{
	/// <summary>
	/// Interface of graph uploading service
	/// </summary>
	[ServiceContract]
	public interface IGraphUploader
	{
		/// <summary>
		/// Clears stored graph
		/// </summary>
		[OperationContract]
		void Clear();

		/// <summary>
		/// Adds node to graph
		/// </summary>
		/// <param name="node">Node to add</param>
		/// <param name="skipExisting">Skip node if is node already stored, otherwise exception is thrown</param>
		[OperationContract]
		void AddNode(NodeContract node, bool skipExisting);

		/// <summary>
		/// Adds edge to graph
		/// </summary>
		/// <param name="edge">Edge to add</param>
		/// <param name="skipExisting">Skip edge if is edge already stored, otherwise exception is thrown</param>
		[OperationContract]
		void AddEdge(EdgeContract edge, bool skipExisting);
	}
}
