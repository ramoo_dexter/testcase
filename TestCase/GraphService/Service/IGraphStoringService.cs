﻿using GraphService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GraphService.Service
{
	/// <summary>
	/// Interface of service for storing nodes and edges
	/// </summary>
	public interface IGraphStoringService
	{
		/// <summary>
		/// Clears stored graph
		/// </summary>
		void Clear();

		/// <summary>
		/// Adds node to store
		/// </summary>
		/// <param name="node">Node to store</param>
		void AddNode(Node node);

		/// <summary>
		/// Adds edge to store
		/// </summary>
		/// <param name="edge">Edge to store</param>
		void AddEdge(Edge edge);

		/// <summary>
		/// Gets node by id
		/// </summary>
		/// <param name="id">Id of node</param>
		/// <returns>Node if found, null if not</returns>
		Node GetNode(int id);

		/// <summary>
		/// Gets all stored nodes
		/// </summary>
		/// <returns></returns>
		IEnumerable<Node> GetNodes();

		/// <summary>
		/// Gets all stored edges
		/// </summary>
		/// <returns></returns>
		IEnumerable<Edge> GetEdges();
	}
}