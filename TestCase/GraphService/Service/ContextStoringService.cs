﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GraphService.Model;
using GraphService.DAL;

namespace GraphService.Service
{
	/// <summary>
	/// Class representing entity framework storage of graph
	/// </summary>
	public class ContextStoringService : IGraphStoringService
	{
		/// <summary>
		/// Entity framework db context
		/// </summary>
		protected GraphStoreContext _graphStoreContext;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="graphStoreContext">Entity framework db context to use</param>
		public ContextStoringService(GraphStoreContext graphStoreContext)
		{
			if (graphStoreContext == null)
				throw new ArgumentNullException("graphStoreContext");

			_graphStoreContext = graphStoreContext; 
		}

		/// <summary>
		/// Adds edge to database
		/// </summary>
		/// <param name="edge">Edge to add</param>
		public void AddEdge(Edge edge)
		{
			if (edge == null)
				throw new ArgumentNullException("edge");

			_graphStoreContext.Edges.Add(edge);
			_graphStoreContext.SaveChanges();
		}

		/// <summary>
		/// Adds node to database
		/// </summary>
		/// <param name="node">Node to add</param>
		public void AddNode(Node node)
		{
			if (node == null)
				throw new ArgumentNullException("node");

			_graphStoreContext.Nodes.Add(node);
			_graphStoreContext.SaveChanges();
		}

		/// <summary>
		/// Clears edges and nodes from database
		/// </summary>
		public void Clear()
		{
			foreach (var edge in _graphStoreContext.Edges)
				_graphStoreContext.Edges.Remove(edge);

			foreach (var node in _graphStoreContext.Nodes)
				_graphStoreContext.Nodes.Remove(node);

			_graphStoreContext.SaveChanges();
		}

		/// <summary>
		/// Returns edges from database
		/// </summary>
		/// <returns>Edges from database</returns>
		public IEnumerable<Edge> GetEdges()
		{
			return _graphStoreContext.Edges.ToList();
		}

		/// <summary>
		/// Returns node by id
		/// </summary>
		/// <param name="id">Id of node to return</param>
		/// <returns>Node if found, otherwise null</returns>
		public Node GetNode(int id)
		{
			return _graphStoreContext.Nodes.Find(id);
		}

		/// <summary>
		/// Returns node from database
		/// </summary>
		/// <returns>Nodes from database</returns>
		public IEnumerable<Node> GetNodes()
		{
			return _graphStoreContext.Nodes.ToList();
		}
	}
}