﻿using GraphService.Contracts;
using GraphService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GraphService.Service
{
	/// <summary>
	/// Class representing graph management service
	/// </summary>
	public class GraphServiceFacade : IGraphServiceFacade
	{
		/// <summary>
		/// Storing service to use
		/// </summary>
		protected IGraphStoringService _graphStoringService;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="graphStoringService">Storing service to use</param>
		public GraphServiceFacade(IGraphStoringService graphStoringService)
		{
			if (graphStoringService == null)
				throw new ArgumentNullException("graphStoringService");

			_graphStoringService = graphStoringService;
		}

		/// <summary>
		/// Adds edge to graph
		/// </summary>
		/// <param name="edge">Edge to add</param>
		/// <param name="skipExisting"></param>
		public void AddEdge(EdgeContract edge, bool skipExisting)
		{
			if (edge == null)
				throw new ArgumentNullException("edge");

			var first = _graphStoringService.GetNode(edge.IdFirst);
			var second = _graphStoringService.GetNode(edge.IdSecond);

			if (first == null)
				throw new InvalidOperationException(string.Format("There is no node with id '{0}'.", edge.IdFirst));

			if (second == null)
				throw new InvalidOperationException(string.Format("There is no node with id '{0}'.", edge.IdSecond));

			var edges = from e in _graphStoringService.GetEdges()
						where
							(e.FirstNode.ID == edge.IdFirst && e.SecondNode.ID == edge.IdSecond) ||
							(e.SecondNode.ID == edge.IdFirst && e.FirstNode.ID == edge.IdSecond)
						select e;

			if (edges.Count() > 0)
			{
				if (skipExisting)
					return;

				throw new InvalidOperationException(string.Format("There is already edge connecting node '{0}' with '{1}'.", edge.IdFirst, edge.IdSecond));
			}

			var toStore = new Edge(first, second);
			_graphStoringService.AddEdge(toStore);
		}

		/// <summary>
		/// Adds node to graph
		/// </summary>
		/// <param name="node">Node to add</param>
		/// <param name="skipExisting"></param>
		public void AddNode(NodeContract node, bool skipExisting)
		{
			if (node == null)
				throw new ArgumentNullException("node");

			var toStore = _graphStoringService.GetNode(node.Id);

			if (toStore != null)
			{
				if (skipExisting)
					return;

				throw new InvalidOperationException(string.Format("There is node with id '{0}'.", node.Id));
			}

			toStore = new Node { ID = node.Id, Label = node.Label };
			_graphStoringService.AddNode(toStore);
		}

		/// <summary>
		/// Clears graph
		/// </summary>
		public void Clear()
		{
			_graphStoringService.Clear();
		}

		/// <summary>
		/// Returns graph
		/// </summary>
		/// <returns>Graph</returns>
		public GraphContract GetGraph()
		{
			var graph = new GraphContract();
			graph.Nodes = new List<NodeContract>();
			graph.Edges = new List<EdgeContract>();

			foreach (var node in _graphStoringService.GetNodes())
				graph.Nodes.Add(new NodeContract { Id = node.ID, Label = node.Label });

			foreach (var edge in _graphStoringService.GetEdges())
				graph.Edges.Add(new EdgeContract { IdFirst = edge.FirstNode.ID, IdSecond = edge.SecondNode.ID });

			return graph;
		}

		/// <summary>
		/// Return node by id
		/// </summary>
		/// <param name="id">Id of node to find</param>
		/// <returns>Found node or null if not found</returns>
		public NodeContract GetNode(int id)
		{
			var node = _graphStoringService.GetNode(id);

			if (node == null)
				return null;

			return new NodeContract { Id = node.ID, Label = node.Label };
		}

		/// <summary>
		/// Calculates shortest path between nodes in graph
		/// </summary>
		/// <param name="idStart">Id of start node</param>
		/// <param name="idEnd">Id of end node</param>
		/// <returns>Calculation result or null if path not found</returns>
		public CalculateContract CalculatePath(int idStart, int idEnd)
		{
			var graph = GetGraph();

			if (!graph.Nodes.Any(n => n.Id == idStart))
				throw new InvalidOperationException(string.Format("There is no node with id '{0}'.", idStart));

			if (!graph.Nodes.Any(n => n.Id == idEnd))
				throw new InvalidOperationException(string.Format("There is no node with id '{0}'.", idEnd));

			if (idStart == idEnd)
				return null;

			var edges = graph.Edges;
			var nodesVisited = new List<NodeVisited>();
			nodesVisited.Add(new NodeVisited { Id = idStart, Previous = idStart });

			var found = false;
			while(edges.Count > 0 && !found)
			{
				found = DoStep(nodesVisited, edges, idEnd);									
			}

			if (!found)
				return null;

			return GetPathFromVisitedNodes(nodesVisited, idStart, idEnd);
		}

		/// <summary>
		/// Creates calculation result based on nodes visited
		/// </summary>
		/// <param name="nodesVisited">Nodes visited</param>
		/// <param name="idStart">Starting node id</param>
		/// <param name="idEnd">Ending node id</param>
		/// <returns>Calculation result</returns>
		private CalculateContract GetPathFromVisitedNodes(List<NodeVisited> nodesVisited, int idStart, int idEnd)
		{
			var nodeChain = new List<NodeContract>();
			AddVisitedNodesToChain(nodesVisited, idStart, idEnd, nodeChain);
			nodeChain.Reverse();

			return new CalculateContract { From = GetNode(idStart), To = GetNode(idEnd), Path = nodeChain.ToArray() };
		}

		/// <summary>
		/// Method recursively builds path
		/// </summary>
		/// <param name="nodesVisited">Visited nodes</param>
		/// <param name="idStart">Starting node id</param>
		/// <param name="current">Current node id</param>
		/// <param name="nodeChain">Chain to add nodes</param>
		private void AddVisitedNodesToChain(List<NodeVisited> nodesVisited, int idStart, int current, List<NodeContract> nodeChain)
		{
			var nodeVisited = nodesVisited.Where(n => n.Id == current).FirstOrDefault();

			if(idStart == current)
			{
				nodeChain.Add(GetNode(nodeVisited.Id));
				return;
			}

			if (nodeVisited != null)
			{
				nodeChain.Add(GetNode(nodeVisited.Id));
				AddVisitedNodesToChain(nodesVisited, idStart, nodeVisited.Previous, nodeChain);
			}
		}

		/// <summary>
		/// Does next step in graph based on nodes visited and available edges to use.
		/// </summary>
		/// <param name="visited">Nodes visited</param>
		/// <param name="edgesAvailable">Edges available to use</param>
		/// <param name="end">End node to find</param>
		/// <returns>True if end node reached, otherwise false</returns>
		private bool DoStep(List<NodeVisited> visited, List<EdgeContract> edgesAvailable, int end)
		{
			var edges = (from e in edgesAvailable where visited.Any(v => v.Id == e.IdFirst || v.Id == e.IdSecond) select e).ToList();

			if (edges.Count() == 0)
			{
				edgesAvailable.Clear();
				return false;
			}

			var visitedInThisStep = new List<NodeVisited>();

			foreach(var edge in edges)
			{
				if(visited.Any(v => v.Id == edge.IdFirst && v.Id != edge.IdSecond))
				{
					visitedInThisStep.Add(new NodeVisited { Id = edge.IdSecond, Previous = edge.IdFirst });
				}
				else if (visited.Any(v => v.Id == edge.IdSecond && v.Id != edge.IdFirst))
				{
					visitedInThisStep.Add(new NodeVisited { Id = edge.IdFirst, Previous = edge.IdSecond });
				}

				if (edge.IdFirst == end || edge.IdSecond == end)
				{
					edgesAvailable = new List<EdgeContract>();
					if(edge.IdFirst == end)
						visited.Add(new NodeVisited { Id = edge.IdFirst, Previous = edge.IdSecond });
					else
						visited.Add(new NodeVisited { Id = edge.IdSecond, Previous = edge.IdFirst });

					return true;
				}
			}

			foreach(var currentlyVisited in visitedInThisStep)
			{
				visited.Add(currentlyVisited);
			}

			for (int i = edgesAvailable.Count - 1; i >= 0; i--)
			{
				foreach(var edge in edges)
				{
					if (edgesAvailable.Count == 0)
						break;

					if(edge.IdFirst == edgesAvailable[i].IdFirst && edge.IdSecond == edgesAvailable[i].IdSecond)
						edgesAvailable.RemoveAt(i);
				}
			}

			 return false;
		}

		/// <summary>
		/// Class for storing information about visited nodes
		/// </summary>
		private class NodeVisited
		{
			/// <summary>
			/// Id of visited node
			/// </summary>
			public Int32 Id { get; set; }

			/// <summary>
			/// Id of entry node
			/// </summary>
			public Int32 Previous { get; set; }
		}
	}
}