﻿using GraphService.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GraphService.Service
{
	/// <summary>
	/// Interface of graph management service
	/// </summary>
	public interface IGraphServiceFacade
	{
		/// <summary>
		/// Clears graph
		/// </summary>
		void Clear();

		/// <summary>
		/// Adds node to graph
		/// </summary>
		/// <param name="node">Node to add</param>
		/// <param name="skipExisting"></param>
		void AddNode(NodeContract node, bool skipExisting);

		/// <summary>
		/// Adds edge to graph
		/// </summary>
		/// <param name="edge">Edge to add</param>
		/// <param name="skipExisting"></param>
		void AddEdge(EdgeContract edge, bool skipExisting);

		/// <summary>
		/// Returns graph
		/// </summary>
		/// <returns>Graph</returns>
		GraphContract GetGraph();

		/// <summary>
		/// Return node by id
		/// </summary>
		/// <param name="id">Id of node to find</param>
		/// <returns>Found node or null if not found</returns>
		NodeContract GetNode(int id);

		/// <summary>
		/// Calculates shortest path between nodes in graph
		/// </summary>
		/// <param name="idStart">Id of start node</param>
		/// <param name="idEnd">Id of end node</param>
		/// <returns>Calculation result or null if path not found</returns>
		CalculateContract CalculatePath(int idStart, int idEnd);
	}
}