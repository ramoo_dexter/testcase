﻿using GraphService.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace GraphService.DAL
{
	/// <summary>
	/// Graph store context for entity framerork.
	/// </summary>
	public class GraphStoreContext : DbContext
	{
		/// <summary>
		/// Nodes stored
		/// </summary>
		public DbSet<Node> Nodes { get; set; }

		/// <summary>
		/// Edges stored
		/// </summary>
		public DbSet<Edge> Edges { get; set; }

		/// <summary>
		/// Constructor
		/// </summary>
		public GraphStoreContext() : base("GraphStoreContext")
		{
			Configuration.ProxyCreationEnabled = false;
			Configuration.LazyLoadingEnabled = false;	
		}

		/// <summary>
		/// Model creating method, setups model definition.
		/// </summary>
		/// <param name="modelBuilder">Model builder to setup</param>
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			if (modelBuilder == null)
				throw new ArgumentNullException("modelBuilder");

			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

			modelBuilder.Entity<Edge>()
				.HasRequired(e => e.FirstNode)
				.WithMany()
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<Edge>()
				.HasRequired(e => e.SecondNode)
				.WithMany()
				.WillCascadeOnDelete(false);
		}
	}
}