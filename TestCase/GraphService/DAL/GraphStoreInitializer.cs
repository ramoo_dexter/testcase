﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GraphService.DAL
{
	/// <summary>
	/// Initializer class for entity framework
	/// </summary>
	public class GraphStoreInitializer : DropCreateDatabaseIfModelChanges<GraphStoreContext>
	{

	}
}