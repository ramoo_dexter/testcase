﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace GraphWeb
{
	/// <summary>
	/// Class used to register style and script bundles.
	/// </summary>
	public class BundleConfig
	{
		/// <summary>
		/// Registers bundles to specified bundle collection.
		/// </summary>
		/// <param name="bundles">Collection to register bundles in</param>
		public static void RegisterBundles(BundleCollection bundles)
		{
			if (bundles == null)
				throw new ArgumentNullException("bundles");

			bundles.Add(new ScriptBundle("~/js/vis").Include("~/Content/vis.js"));
			bundles.Add(new StyleBundle("~/css/vis").Include("~/Content/vis.css"));
			bundles.Add(new ScriptBundle("~/js/jquery").Include(
				"~/Scripts/jquery-{version}.js",
				"~/Scripts/jquery.validate.js",
				"~/Scripts/jquery.validate.unobtrusive.js",
				"~/Scripts/jquery.unobtrusive-ajax.js"));
		}
	}
}