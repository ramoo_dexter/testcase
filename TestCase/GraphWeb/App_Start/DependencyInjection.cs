﻿using Autofac;
using Autofac.Integration.Mvc;
using GraphWeb.GraphReader;
using GraphWeb.GraphService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GraphWeb
{
	/// <summary>
	/// Class used to build dependency injection container.
	/// </summary>
	public class DependencyInjection
	{
		/// <summary>
		/// Registers dependencies to DI container. Sets dependency resolver to use Autofac.
		/// </summary>
		public static void RegisterDependencies()
		{
			var builder = new ContainerBuilder();
			builder.Register(s => new GraphReaderClient())
				.As<IGraphReader>()
				.InstancePerLifetimeScope();
			builder.Register(s => new GraphServiceClient())
				.As<IGraphService>()
				.InstancePerLifetimeScope();
			builder.RegisterControllers(typeof(MvcApplication).Assembly);

			var container = builder.Build();
			DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
		}
	}
}