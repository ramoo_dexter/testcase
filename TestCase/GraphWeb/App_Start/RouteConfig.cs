﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace GraphWeb
{
	/// <summary>
	/// Class used to configure appliaction routes.
	/// </summary>
	public class RouteConfig
    {
		/// <summary>
		/// Maps routes to specified route collection.
		/// </summary>
		/// <param name="routes">Route collection to map routes in.</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
			if (routes == null)
				throw new ArgumentNullException("routes");

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}",
                defaults: new { controller = "Graph", action = "Index" }
            );
        }
    }
}
