﻿using GraphWeb.GraphReader;
using GraphWeb.GraphService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GraphWeb.Models
{
	/// <summary>
	/// Class representing calculation result for result page.
	/// </summary>
	public class GraphCalculationResultViewModel
	{
		/// <summary>
		/// Result to display
		/// </summary>
		public CalculateContract CalculateContract { get; private set; }

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="calculateContract">Result to display</param>
		public GraphCalculationResultViewModel(CalculateContract calculateContract)
		{
			if (calculateContract == null)
				throw new ArgumentNullException("calculateContract");

			CalculateContract = calculateContract;
		}
	}
}