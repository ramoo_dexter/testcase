﻿using GraphWeb.GraphService;
using GraphWeb.GraphReader;
using GraphWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GraphWeb.Controllers
{
	/// <summary>
	/// Class representing graph controller. Handles user requests for view graph and calculate shortest path in graph.
	/// </summary>
	public class GraphController : Controller
	{
		/// <summary>
		/// Service for calculating shortest path in graph
		/// </summary>
		protected IGraphService _graphService;

		/// <summary>
		/// Service for getting graph structure
		/// </summary>
		protected IGraphReader _graphReader;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="graphService">Calculation service</param>
		/// <param name="graphReader">Graph structure retrieving service</param>
		public GraphController(IGraphService graphService, IGraphReader graphReader)
		{
			if (graphService == null)
				throw new ArgumentNullException("graphService");

			if (graphReader == null)
				throw new ArgumentNullException("graphReader");

			_graphService = graphService;
			_graphReader = graphReader;
		}

		/// <summary>
		/// Index action displays graph structure.
		/// </summary>
		/// <returns>View with graph structure</returns>
		[HttpGet]
		public ActionResult Index()
		{
			var graph = _graphReader.GetGraph();

			return View(graph);
		}

		/// <summary>
		/// Calculates shortest path between specified nodes.
		/// </summary>
		/// <param name="idFirst">Id of first node</param>
		/// <param name="idSecond">Id of second node</param>
		/// <returns>View with graph calculation result</returns>
		[HttpPost]
		public ActionResult CalculateShortestPath(int idFirst, int idSecond)
		{
			var first = _graphReader.GetNode(idFirst);
			var second = _graphReader.GetNode(idSecond);

			if (first == null || second == null || idFirst == idSecond)
				return PartialView("Message", "Select two different nodes.");

			var contract = _graphService.CalculatePath(idFirst, idSecond);

			if(contract == null)
				return PartialView("Message", "Path not found.");

			return PartialView(new GraphCalculationResultViewModel(contract));
		}
	}
}