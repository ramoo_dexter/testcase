﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace GraphWeb
{
	/// <summary>
	/// Application bootstrap class.
	/// </summary>
    public class MvcApplication : System.Web.HttpApplication
    {
		/// <summary>
		/// Application start method used for prepare web application.
		/// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			DependencyInjection.RegisterDependencies();
		}
    }
}
