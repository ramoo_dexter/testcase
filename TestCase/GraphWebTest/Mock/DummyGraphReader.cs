﻿using GraphWeb.GraphReader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphWebTest.Mock
{
	/// <summary>
	/// Class representing mock graph reader. 
	/// </summary>
	public class DummyGraphReader : IGraphReader
	{
		/// <summary>
		/// Returns predefined graph. Graph structure is: 1 - 2 - 3  4
		/// </summary>
		/// <returns>Predefined graph</returns>
		public GraphContract GetGraph()
		{
			// 1 - 2 - 3   4
			var nodes = new List<NodeContract>();
			nodes.Add(new NodeContract { Id = 1, Label = "test1" });
			nodes.Add(new NodeContract { Id = 2, Label = "test2" });
			nodes.Add(new NodeContract { Id = 3, Label = "test3" });
			nodes.Add(new NodeContract { Id = 4, Label = "test4" });

			var edges = new List<EdgeContract>();
			edges.Add(new EdgeContract { IdFirst = 1, IdSecond = 2 });
			edges.Add(new EdgeContract { IdFirst = 2, IdSecond = 3 });

			return new GraphContract { Nodes = nodes.ToArray(), Edges = edges.ToArray() };
		}

		/// <summary>
		/// Not implemented method for getting graph by id asynchronously
		/// </summary>
		/// <returns>Not implemented exception</returns>
		public Task<GraphContract> GetGraphAsync()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Not implemented method for getting node by id
		/// </summary>
		/// <param name="id">Id of node</param>
		/// <returns>Not implemented exception</returns>
		public NodeContract GetNode(int id)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Not implemented method for getting node by id asynchronously
		/// </summary>
		/// <param name="id">Id of node</param>
		/// <returns>Not implemented exception</returns>
		public Task<NodeContract> GetNodeAsync(int id)
		{
			throw new NotImplementedException();
		}
	}
}
