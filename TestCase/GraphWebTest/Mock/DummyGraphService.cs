﻿using GraphWeb.GraphService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphWebTest.Mock
{
	/// <summary>
	/// Class representing mock graph service. Contains not implemented service interface.
	/// </summary>
	public class DummyGraphService : IGraphService
	{
		/// <summary>
		/// Not implemented method for calculating path between two nodes.
		/// </summary>
		/// <param name="idStart">Start node id</param>
		/// <param name="idEnd">End note id</param>
		/// <returns>Throws not implemented exception</returns>
		public CalculateContract CalculatePath(int idStart, int idEnd)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Not implemented method for calculating path between two nodes asynchronously.
		/// </summary>
		/// <param name="idStart">Start node id</param>
		/// <param name="idEnd">End note id</param>
		/// <returns>Throws not implemented exception</returns>
		public Task<CalculateContract> CalculatePathAsync(int idStart, int idEnd)
		{
			throw new NotImplementedException();
		}
	}
}
