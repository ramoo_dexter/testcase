﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphWeb.Controllers;
using GraphWebTest.Mock;
using System.Web.Mvc;
using GraphWeb.GraphReader;

namespace GraphWebTest
{
	/// <summary>
	/// Test class for testing graph controller.
	/// </summary>
	[TestClass]
	public class GraphControllerTest
	{
		/// <summary>
		/// Test testing correct load of index page.
		/// </summary>
		[TestMethod]
		public void IndexAction()
		{
			var controller = new GraphController(new DummyGraphService(), new DummyGraphReader());
			var result = controller.Index() as ViewResult;

			Assert.IsNotNull(result.Model);
			Assert.IsInstanceOfType(result.Model, typeof(GraphContract));
		}

		/// <summary>
		/// Test testing null service argument.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void GraphControllerNullServiceArgument()
		{
			var controller = new GraphController(null, new DummyGraphReader());
		}

		/// <summary>
		/// Test testing null loader argument.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void GraphControllerNullReaderArgument()
		{
			var controller = new GraphController(new DummyGraphService(), null);
		}
	}
}
