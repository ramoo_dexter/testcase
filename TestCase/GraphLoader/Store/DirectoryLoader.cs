﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphLoader.Model;
using System.IO;
using System.Xml.Serialization;

namespace GraphLoader.Store
{
	/// <summary>
	/// Class implementing graph loader, loading edges and nodes from set of files.
	/// </summary>
	public class DirectoryLoader : IGraphLoader
	{
		/// <summary>
		/// Loaded nodes
		/// </summary>
		protected List<Node> _nodes;

		/// <summary>
		/// Loaded edges
		/// </summary>
		protected List<Edge> _edges;

		/// <summary>
		/// Source directory info
		/// </summary>
		protected DirectoryInfo _info;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="path">Directory containing input files</param>
		public DirectoryLoader(string path)
		{
			_info = new DirectoryInfo(path);

			if (!_info.Exists)
				throw new InvalidOperationException(string.Format("Folder '{0}' does not exist.", path));

			Load();
		}

		/// <summary>
		/// Returns loaded nodes from source directory.
		/// </summary>
		/// <returns>Nodes loaded</returns>
		public IEnumerable<Node> LoadNodes()
		{
			return _nodes;
		}

		/// <summary>
		/// Returns loaded edges from source directory.
		/// </summary>
		/// <returns>Edges loaded</returns>
		public IEnumerable<Edge> LoadEdges()
		{
			return _edges;
		}

		/// <summary>
		/// Internal loading method used to load all nodes and edges.
		/// </summary>
		private void Load()
		{
			_nodes = new List<Node>();
			var serializer = new XmlSerializer(typeof(Node));

			foreach (var file in _info.GetFiles("*.xml"))
			{
				using (var stream = file.OpenRead())
				{
					var node = (Node)serializer.Deserialize(stream);
					_nodes.Add(node);
				}
			}

			_edges = new List<Edge>();

			foreach (var node in _nodes)
			{
				foreach (var adjacent in node.AdjacentNodes)
					_edges.Add(new Edge { FirstNodeId = node.Id, SecondNodeId = adjacent });
			}
		}
	}
}
