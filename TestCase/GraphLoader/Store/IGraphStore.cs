﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphLoader.Store
{
	/// <summary>
	/// Interface representing graph store.
	/// </summary>
	public interface IGraphStore
	{
		/// <summary>
		/// Clears all stored nodes and edges
		/// </summary>
		void Clear();

		/// <summary>
		/// Adds node
		/// </summary>
		/// <param name="id">Id of node</param>
		/// <param name="label">Label of node</param>
		void AddNode(int id, string label);

		/// <summary>
		/// Adds edge
		/// </summary>
		/// <param name="idFirst">Id of first node</param>
		/// <param name="idSecond">Id of second node</param>
		void AddEdge(int idFirst, int idSecond);
	}
}
