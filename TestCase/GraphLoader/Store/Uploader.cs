﻿using GraphLoader.GraphUploader;
using GraphLoader.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GraphLoader.Store
{
	/// <summary>
	/// Class for loading nodes and edges using specific loader class and uploading them to specific store.
	/// </summary>
	public class Uploader
	{
		/// <summary>
		/// Upload method loads nodes and edges using specific loader and uploads them to specific store. 
		/// </summary>
		/// <param name="graphStore">Store, where should be loaded nodes and edges stored.</param>
		/// <param name="graphLoader">Loader for nodes and edges.</param>
		public void UploadGraph(IGraphStore graphStore, IGraphLoader graphLoader)
		{
			if (graphStore == null)
				throw new ArgumentNullException("graphStore");

			if (graphLoader == null)
				throw new ArgumentNullException("graphLoader");

			var nodes = graphLoader.LoadNodes();
			var edges = graphLoader.LoadEdges();

			graphStore.Clear();

			foreach (var node in nodes)
			{
				graphStore.AddNode(node.Id, node.Label);
			}

			foreach (var edge in edges)
			{
				graphStore.AddEdge(edge.FirstNodeId, edge.SecondNodeId);
			}
		}
	}
}
