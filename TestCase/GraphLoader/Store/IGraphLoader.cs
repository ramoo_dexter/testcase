﻿using GraphLoader.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphLoader.Store
{
	/// <summary>
	/// Interface representing loader of nodes and edges.
	/// </summary>
	public interface IGraphLoader
	{
		/// <summary>
		/// Loads nodes
		/// </summary>
		/// <returns>Nodes loaded</returns>
		IEnumerable<Node> LoadNodes();

		/// <summary>
		/// Loads edges
		/// </summary>
		/// <returns>Edges loaded</returns>
		IEnumerable<Edge> LoadEdges();
	}
}
