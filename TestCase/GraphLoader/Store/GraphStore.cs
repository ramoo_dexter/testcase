﻿using GraphLoader.GraphUploader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphLoader.Store
{
	/// <summary>
	/// Class representing store for graph using graph uploading service.
	/// </summary>
	public class GraphStore : IGraphStore
	{
		/// <summary>
		/// Service to use for uploading
		/// </summary>
		protected IGraphUploader _graphUploader;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="graphUploader">Service to use for uploading</param>
		public GraphStore(IGraphUploader graphUploader)
		{
			if (graphUploader == null)
				throw new ArgumentNullException("graphUploader");

			_graphUploader = graphUploader;
		}

		/// <summary>
		/// Uploads edge using service
		/// </summary>
		/// <param name="idFirst">Id of first node</param>
		/// <param name="idSecond">Id of second node</param>
		public void AddEdge(int idFirst, int idSecond)
		{
			_graphUploader.AddEdge(new EdgeContract { IdFirst = idFirst, IdSecond = idSecond }, true);
		}

		/// <summary>
		/// Uploads node using service
		/// </summary>
		/// <param name="id">Id of node</param>
		/// <param name="label">Label of node</param>
		public void AddNode(int id, string label)
		{
			_graphUploader.AddNode(new NodeContract { Id = id, Label = label }, true);
		}

		/// <summary>
		/// Clears store using service
		/// </summary>
		public void Clear()
		{
			_graphUploader.Clear();
		}
	}
}
