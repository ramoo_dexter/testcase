﻿using CommandLine;
using GraphLoader.GraphUploader;
using GraphLoader.Model;
using GraphLoader.Store;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GraphLoader
{
	/// <summary>
	/// Program for uploading nodes and edges to graph store. Uploads items from specified directory to graph store using wcf graph store service.
	/// </summary>
	class Program
	{
		/// <summary>
		/// Main method used to run program.
		/// </summary>
		/// <param name="args">Arguments for console appliaction</param>
		static void Main(string[] args)
		{
			var options = new Options();
			if (Parser.Default.ParseArguments(args, options))
			{
				var store = new GraphStore(new GraphUploaderClient());
				var loader = new DirectoryLoader(options.Directory);
				var uploader = new Uploader();
				uploader.UploadGraph(store, loader);
			}
		}
	}
}
