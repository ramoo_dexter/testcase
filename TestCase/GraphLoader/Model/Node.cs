﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GraphLoader.Model
{
	/// <summary>
	/// Class representing node in graph. Is used for deserializing of input files. 
	/// </summary>
	[Serializable]
	[XmlRoot("node")]
	public class Node
	{
		/// <summary>
		/// Id of node
		/// </summary>
		[XmlElement("id")]
		public Int32 Id { get; set; }

		/// <summary>
		/// Label of node
		/// </summary>
		[XmlElement("label")]
		public string Label { get; set; }

		/// <summary>
		/// List of ids of adjacent nodes
		/// </summary>
		[XmlArray("adjacentNodes")]
		[XmlArrayItem("id", typeof(Int32))]
		public List<Int32> AdjacentNodes { get; set; }
	}
}
