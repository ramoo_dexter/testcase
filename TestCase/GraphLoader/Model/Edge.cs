﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphLoader.Model
{
	/// <summary>
	/// Container class representing edge in graph.
	/// </summary>
	public class Edge
	{
		/// <summary>
		/// First node id
		/// </summary>
		public Int32 FirstNodeId { get; set; }

		/// <summary>
		/// Second node id
		/// </summary>
		public Int32 SecondNodeId { get; set; }
	}
}
