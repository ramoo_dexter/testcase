﻿using CommandLine;
using CommandLine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphLoader
{
	/// <summary>
	/// Options class used by console application
	/// </summary>
	public class Options
	{
		/// <summary>
		/// Directory to load nodes and edges from
		/// </summary>
		[Option('d', "directory", Required = true, HelpText = "Directory containing sources for loading graph.")]
		public string Directory { get; set; }

		/// <summary>
		/// State of parameters parser
		/// </summary>
		[ParserState]
		public IParserState LastParserState { get; set; }

		/// <summary>
		/// Help for console application
		/// </summary>
		/// <returns></returns>
		[HelpOption]
		public string GetUsage()
		{
			return HelpText.AutoBuild(this,
				(HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
		}
	}
}
